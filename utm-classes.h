// Written by DaNuk (DNk) 02-03-2006
// mail@danuk.ru

// Direction of traffic
#define DIR_INCOMING 1
#define DIR_OUTGOING 2

#define CLASS_NOT_FOUND 0

struct struct_class_data
{
	unsigned int srcip;
        unsigned int srcmask;
        unsigned int dstip;
        unsigned int dstmask;
	unsigned int class_name;	// User defined class ID (name). 
	unsigned int class_id;		// Internal system class id for calculate offset in memory
};

