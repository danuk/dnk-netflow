// Written by DaNuk (DNk)
// mail@danuk.ru

/* Version number, for the -v command line switch */
#define UTM_NETFLOW_VERSION "UTM-netflow version 2.0.6 (22/03/08), written by DaNuk (DNk), mail@danuk.ru"
/* Default size of socket buffer. FreeBSD default is Limited to 256k,
 * but misteriously enough maximum space available for buf is 217088 */
#define DEFAULT_SOCKBUF                (217088)
 /* Reasonable minimum socket buffer size. I assume it's about 16k */
#define MIN_SOCKBUF            (16*1024)
/* LOG_LEVEL can be each LOG_LOCAL[0-7]. See man syslog.conf  */
#define LOG_LEVEL	LOG_LOCAL0

/* Writen agregate traffic to FILE_DUMP_TMP and rename to FILE_DUMP after */
#define FILE_DUMP_TMP	"dump.tmp"
#define FILE_DUMP	"dump.txt"

#define FILE_NETFLOW_TMP	"netflow.tmp"
#define FILE_NETFLOW		"netflow.txt"

#define FILE_AS_TMP	"as.tmp"
#define FILE_AS		"as.txt"

#define DEFAULT_UDP_PORT        9999

#define DEFAULT_CONFIG_FILE "config"

#define MAX_AGGREGATE_NETS 255
#define MAX_AGGREGATE_AS 255
#define MAX_CLASSES_COUNT 255

