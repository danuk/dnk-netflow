PREFIX=/opt
VERSION=2.0.6
LOCALSTATEDIR=/var/run/utm-netflow
CC = gcc 
CFLAGS = -D_FILE_OFFSET_BITS=64 -ggdb
LIBS = 

all: utm-netflow
#	strip -s utm-netflow
	rm -f *.o

utm-data.o : utm-data.c utm-data.h
	$(CC) -c utm-data.c $(CFLAGS)

utm-config.o : utm-config.c
	$(CC) -c utm-config.c $(CFLAGS)

utm-classes.o : utm-classes.c utm-classes.h
	$(CC) -c utm-classes.c $(CFLAGS)

utm-as.o : utm-as.c utm-as.h
	$(CC) -c utm-as.c $(CFLAGS)

utm-netflow.o : utm-netflow.c netflowv5.h config.h utm-classes.h utm-data.h
	$(CC) -c utm-netflow.c $(CFLAGS)

utm-netflow : utm-netflow.o utm-classes.o utm-data.o utm-config.o utm-as.o
	$(CC) $(CFLAGS) -o utm-netflow utm-netflow.o utm-classes.o utm-data.o utm-config.o utm-as.o $(LIBS)

install:
	mkdir -p $(PREFIX)/utm-netflow/$(VERSION)/bin
	mkdir -p $(PREFIX)/utm-netflow/$(VERSION)/etc
	mkdir -p $(PREFIX)/utm-netflow/$(VERSION)/conf
	mkdir -p $(PREFIX)/utm-netflow/$(VERSION)/traffic
	cp utm-netflow $(PREFIX)/utm-netflow/$(VERSION)/bin/
	cp utm-netflow.sh $(PREFIX)/utm-netflow/$(VERSION)/etc/
	test -f $(PREFIX)/utm-netflow/$(VERSION)/conf/config || cp config $(PREFIX)/utm-netflow/$(VERSION)/conf/

clean : 
	rm -f *.o *~ utm-netflow

