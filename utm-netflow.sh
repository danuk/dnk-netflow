#!/bin/bash

# UTM-netflow version 2.0.6, written by DaNuk (DNk), mail@danuk.ru

PREFIX=/opt
VERSION=2.0.6
filepath=$PREFIX/utm-netflow/$VERSION/traffic
utm_netflow=$PREFIX/utm-netflow/$VERSION/bin/utm-netflow
config=$PREFIX/utm-netflow/$VERSION/conf/config

function start()
{
		if [ ! -d "$filepath" ]; then
		        echo "Error: directory $filepath not exist"
		        exit 1
		fi

		# For debug (core files)
		ulimit -c unlimited
		
		cd $filepath && $utm_netflow -c $config
		exit 0;
}

function stop()
{
		echo -n "Stop utm-netflow... "
		killall utm-netflow > /dev/null 2>&1
		if [ $? -eq '0' ]; then
			echo "done"
		else
			echo "error"
			exit 1
		fi
}

function restart()
{
	stop;
	sleep 5;
	start;
}

case "$1" in
	start)
		start;
        ;;
	top)
		cd $filepath && $utm_netflow -D
                exit 0;
	;;
	stop)
		stop;
	;;
	restart)
		restart;
	;;
	flush)
		echo -n "Flush data utm-netflow... "
		killall -USR1 utm-netflow > /dev/null 2>&1
		if [ $? -eq '0' ]; then
			echo "done"
			exit 0
		else
			echo "error"
			exit 1
		fi
	;;

	*)
		echo "Usage: ./utm-netflow.sh [start|stop|restart|flush]"
	;;
esac
exit 1;


