#include <arpa/inet.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

struct flow_ver5_hdr {
	uint16_t version;
	uint16_t count;
	uint32_t uptime;
	uint32_t unix_secs;
	uint32_t unix_nsecs;
	uint32_t flow_sequence;
	uint8_t  engine_type;
	uint8_t  engine_id;
	};
struct flow_ver5_rec {
	uint32_t srcaddr;
	uint32_t dstaddr;
	uint32_t nexthop;
	uint16_t input_index;
	uint16_t output_index;
	uint32_t dPkts;
	uint32_t dOctets;
	uint32_t First;
	uint32_t Last;
	uint16_t srcport;
	uint16_t dstport;
	uint8_t unused2;
	uint8_t tcp_flags;
	uint8_t prot;
	uint8_t tos;
	uint16_t src_as;
	uint16_t dst_as;
	uint8_t dst_mask;
	uint8_t src_mask;
	ushort pad2;
	};
