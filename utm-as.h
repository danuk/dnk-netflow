// Written by DaNuk (DNk) 02-03-2006
// mail@danuk.ru

#define AS_INCOMING 0
#define AS_OUTGOING 1

struct struct_as
{
	unsigned int number;
	unsigned long long in;
	unsigned long long out;
};

