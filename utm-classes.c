// Writen by DaNuk (DNk) 03-03-2006
// mail@danuk.ru

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>

#include "utm-classes.h" 
#include "config.h" 

int classes[MAX_CLASSES_COUNT];
int classes_index = 0;
int count_classes = 0; // Internal system class id for calculate offset in memory

unsigned int c_name[MAX_CLASSES_COUNT];

extern int debug;

void add_class(unsigned int * srcip, unsigned int * srcmask, 
	       unsigned int * dstip, unsigned int * dstmask, unsigned int * class)
{
	if (MAX_CLASSES_COUNT == classes_index)
	{
		fprintf(stderr,"Error: MAX_CLASSES_COUNT reached\n");
		exit(1);
	}

	struct struct_class_data * class_data;
	class_data = malloc(sizeof(struct struct_class_data));

	if (class_data == NULL)
	{
		fprintf(stderr,"Error: Can't allocated memory size for 'CLASS'\n");
		exit(1);
	}
	
	class_data->srcip	= (int) srcip;
	class_data->srcmask	= (int) srcmask;
	class_data->dstip	= (int) dstip;
	class_data->dstmask	= (int) dstmask;
	class_data->class_name	= (int) class;
	class_data->class_id 	= (int) get_class_id((unsigned int) class);
	
	classes[classes_index] = (int) class_data;
	classes_index++;
	classes[classes_index]=0;
}

int get_class_id(unsigned int * class_name)
{
	struct struct_class_data * class;

	int i = 0;

	while (classes[i]!=0)
	{
		class = (struct struct_class_data *) classes[i];
		if (class->class_name == (int) class_name) return class->class_id;
		i++;
	}

	c_name[count_classes] = (unsigned int) class_name;
	
	return count_classes++;
}

struct struct_class_data *  search_class(unsigned int * srcip, unsigned int * dstip)
{
	int i = 0;	
	struct struct_class_data * class_data;
	
	while ( classes[i] != 0 )
	{
		class_data = (struct struct_class_data *) classes[i];

		if ( (( (int)srcip & class_data->srcmask) == class_data->srcip) &&
		     (( (int)dstip & class_data->dstmask) == class_data->dstip) 
		   ) return class_data;
		i++;
	}
	return CLASS_NOT_FOUND;
}

int get_ip_int(char * sip)
{
	struct in_addr * ipaddr;
	ipaddr = (struct in_addr*) malloc(sizeof(struct in_addr));

	if ( inet_aton(sip,ipaddr) == 0 )
	{
		printf("Incorrect addres: %s\n",sip);
		exit(1);
	}	
	return htonl(ipaddr->s_addr);
}

