// utm-netflow 2.0.0 Writen by DaNuk (DNk) 12-03-2008
// mail@danuk.ru

#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdlib.h>

#include "config.h"
#include "utm-as.h"

extern int human;
extern int debug;
FILE * FILE_AS_TMP_FD = NULL;

//struct struct_as * as_aggregate;
//unsigned int as_aggregate_index = 0;

int as_aggregate[MAX_AGGREGATE_AS];
int as_aggregate_index = 0;

void log_error(const char *);
void log_warning(const char *);

void open_file_as_tmp();

void init_as()
{
     as_aggregate[0] = 0;

}

void aggregate_as(unsigned int * src_as, unsigned int * dst_as, unsigned int * bytes)
{	
	unsigned int as;
	
	if ( src_as == 0 )
	{
		store_as(dst_as,AS_OUTGOING,bytes);
	}
	else
	{
		store_as(src_as,AS_INCOMING,bytes);
	} 

}

struct struct_as * get_as(unsigned int * as)
{
	int i = 0;

	struct struct_as * sas;
	
	while (as_aggregate[i]!=0)
	{		
		sas = (struct struct_as * ) as_aggregate[i];

		if (sas->number == (unsigned int)as) return sas;

		i++;
	}

	if (MAX_AGGREGATE_AS == as_aggregate_index)
	{
		return 0;
	}

	as_aggregate[as_aggregate_index] = (int) malloc(sizeof(struct struct_as));

	if ( as_aggregate[as_aggregate_index] == 0 )
	{
		fprintf(stderr,"Error: Can't allocated memory size for 'AS' storage: %u bytes.\n",
				MAX_AGGREGATE_AS*sizeof(struct struct_as));
		exit(1);
	}

	sas = (struct struct_as * ) as_aggregate[as_aggregate_index];
	
	sas->number = (unsigned int) as;
	sas->in = 0;
	sas->out = 0;
	
	as_aggregate_index++;
	as_aggregate[as_aggregate_index]=0;
	
	return sas;
}


int store_as(unsigned int * as, unsigned int * direction, unsigned int * bytes)
{
	struct struct_as * sas = get_as(as);

	if (sas==0)
	{
		//log_waring("WARNING: 'AS' cache reached!\n");
		fprintf(stderr,"WARNING: 'AS' cache reached!\n");	
		return 0;
	}
	
	if ((unsigned int)direction == AS_INCOMING)
	{	
		sas->in += (unsigned int) bytes;
	}
	else if ((unsigned int)direction == AS_OUTGOING)
	{
		sas->out += (unsigned int) bytes;
	}
}

void write_as_tmp()
{
	if (debug) printf("Write 'AS' to file...");

	open_file_as_tmp();
	
	int i = 0;

	struct struct_as * sas;
	
	while (as_aggregate[i]!=0)
	{		
		sas = (struct struct_as*) as_aggregate[i];

		fprintf(FILE_AS_TMP_FD,"%u\t0\t%llu\n",sas->number,sas->in);	
		fprintf(FILE_AS_TMP_FD,"%u\t1\t%llu\n",sas->number,sas->out);

		sas->in=0;
		sas->out=0;	

		i++;
	}

	fflush(FILE_AS_TMP_FD);
	
	int ret = fclose(FILE_AS_TMP_FD);
	if (ret != 0) log_error("Close file");

	FILE_AS_TMP_FD = NULL;
	
	if (debug) printf("Done\n");
}


void rename_as_tmp()
{
	if (FILE_AS_TMP_FD == NULL)
	{
		int ret = rename(FILE_AS_TMP,FILE_AS);
		if (ret != 0) log_error("Rename file");
	}
	else
	{
		log_error("Can't rename 'AS' file\n");
	}
}

void open_file_as_tmp()
{
	if (FILE_AS_TMP_FD == NULL)
	{
		FILE_AS_TMP_FD = fopen(FILE_AS_TMP, "a");
	        if (FILE_AS_TMP_FD == NULL) log_error("Create as file");
	}
}


