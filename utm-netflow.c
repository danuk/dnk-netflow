// Written by DaNuk (DNk) 02-03-2006
// mail@danuk.ru

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/time.h>  
#include <netinet/in.h>
#include <arpa/inet.h> 
#include <sys/un.h> /* sockaddr_un */
#include <errno.h>
#include <netdb.h>
#include <signal.h>
#include <netinet/tcp.h>
#include <syslog.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>

#include "netflowv5.h"
#include "config.h"
#include "utm-classes.h"

#define MAXPACKET 1464 // Header_size + rec_size * 30
#define uc (unsigned char)

char * CONFIG_FILE = DEFAULT_CONFIG_FILE;
char * BIND_ADDRESS = 0;
int UDP_PORT = DEFAULT_UDP_PORT;

int SetUDPOpts(int, int);

int sig_lock_usr1=0;
int sig_lock_usr2=0;
int debug=0;
int debug2=0;
int use_syslog=1;
int human=0;
int not_detalized=0;
int as_aggregate_on=0;

FILE * fd = NULL; //file stream descriptor

struct struct_class_data * class;

void log_error(const char *);
void log_warning(const char *);

void processmessage(int sockfd);
	
void close_dump_file_tmp()
{
	if (fd!=NULL)
	{
		if (debug) printf ("Flush to file %s\n",FILE_NETFLOW);
		fflush(fd);

		int ret = fclose(fd);	
		if (ret != 0) log_error("Close file");
	}
	else
	{
		log_error("Error: dump file not opened or not exist");
	}

	fd = NULL;
	return;
}

void rename_dump_file_tmp()
{
	int ret=rename(FILE_NETFLOW_TMP,FILE_NETFLOW);
	if (ret !=0) log_error("Rename file");
}

void create_dump_file_tmp()
{
	if (fd == NULL)
	{
		fd = fopen(FILE_NETFLOW_TMP,"a");
		if (fd == NULL) log_error("Create dump file");
	}
	else
	{
		log_error("File already opened");
	}
	return;
}

void callsig(int signal_number)
{
	switch(signal_number) {
	case SIGUSR1:
		if (sig_lock_usr1!=0)
		{
			syslog(LOG_INFO, "USR1 signal in progress. SIGNAL IS LOCKED!");
			return;
		}
		// Lock
	        sig_lock_usr1=1;
				
		if (!not_detalized)
		{
			syslog(LOG_INFO, "Flush to file %s",FILE_NETFLOW);

			close_dump_file_tmp();
			rename_dump_file_tmp();
			create_dump_file_tmp();
		}

		syslog(LOG_INFO, "Write aggregate billing data to file %s", FILE_DUMP);

		write_data_tmp();
		syslog(LOG_INFO, "Write aggregate billing data: OK");

		rename_data_tmp();
		syslog(LOG_INFO, "Rename aggregate billing data: OK");
		// Unlock
		sig_lock_usr1=0;
		break;

	case SIGUSR2:

		if (sig_lock_usr2!=0)
	        {
	                syslog(LOG_INFO, "USR2 signal in progress. SIGNAL IS LOCKED!");
	                return;
	        }
	        sig_lock_usr2=1;
				
                if (as_aggregate_on)
		{
			write_as_tmp();
			rename_as_tmp();
			syslog(LOG_INFO, "Write aggregate AS data: OK");
		}
		else
		{
			syslog(LOG_INFO, "AS aggregate is not included in configuration.");
		}

		sig_lock_usr2=0;
		
		break;

	default:
		if ( sig_lock_usr1 == 1 || sig_lock_usr2 == 1 )
		{
	                syslog(LOG_INFO, "Can`t shutting down utm-netflow, signals in progress");
	                fprintf(stderr, "Can`t shutting down utm-netflow, signals in progress\n");
			break;
		}
		if (!not_detalized) close_dump_file_tmp();

		if (as_aggregate_on) write_as_tmp();

		write_data_tmp();

		syslog(LOG_INFO, "Shutting down utm-netflow");
		fprintf(stderr, "Shutting down utm-netflow\n");
		exit(0);
	}
	return;
}

int main(int argc, char **argv) {
	int c,i;

	int sockfd;  /*NetFlow fd*/
	struct sockaddr_in nf_servaddr, nf_cliaddr, l_servaddr, l_cliaddr;
	struct sockaddr_un ul_servaddr;
	socklen_t nf_clilen,l_clilen;
	char nf_packet[MAXPACKET];
	extern char *optarg;
	int bufsize = DEFAULT_SOCKBUF;

	
	while( (c=getopt(argc,argv,"c:t:s:adDmveh")) != -1) 
	{
		switch(c)
		{
		case 'c':
			CONFIG_FILE = strdup(optarg);
			break;
		case 's':
			bufsize = atoi(optarg);
	
			if (bufsize < MIN_SOCKBUF)
			{
				fprintf(stderr,"too low buffer size or bad format: %s\n", optarg);
				exit(1);
			}
			break;
		case 'v':
			printf("%s\n",UTM_NETFLOW_VERSION);
			exit(0);
		case 'd':
			debug=1;
			break;
		case 'D':
			debug2=1;
			break;
		case 'm':
			human=1;
			break;
		case 'e':
			not_detalized=1;
			break;
		case 'a':
			as_aggregate_on=1;
			break;
		default:

		printf("%s\n",UTM_NETFLOW_VERSION);
		printf("Usage: %s [ -u <udpport> ]\n",argv[0]);
		printf("  -c <config>   Set config file name. Default: %s\n",DEFAULT_CONFIG_FILE);
		printf("  -s <bufsize>  Set udp buffer size.  Default: %d\n",DEFAULT_SOCKBUF);
		printf("  -m		Show human format ip address\n");
		printf("  -D		Interactive mode (not daemon)\n");
		printf("  -e		No write detalized file\n");
		printf("  -a		AS aggregate mode\n");
		printf("  -v		Print version\n");
		printf("  -d            Debug.  Don't become a daemon, displays all messages on stdout\n");
		exit(0);
		}
	}
	
	if (debug == 1)
	{
		openlog("utm-netflow", (LOG_PERROR | LOG_PID), LOG_LEVEL);
	}
	else
	{
		openlog("utm-netflow", LOG_PID, LOG_LEVEL);
	}

	syslog(LOG_INFO, "Starting utm-netflow");

	init_config(CONFIG_FILE);
	init_data();
	if (as_aggregate_on) init_as();
	
	/* set up the netflow udp socket */
	if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) { log_error("socket (udp) error"); }
	memset(&nf_servaddr,0,sizeof(nf_servaddr));
	nf_servaddr.sin_family      = AF_INET;
	
	if (BIND_ADDRESS == 0)
	{
		nf_servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	}
	else
	{
		struct in_addr bind_address;
		inet_aton(BIND_ADDRESS,&bind_address);
		nf_servaddr.sin_addr.s_addr = bind_address.s_addr;
	}
	nf_servaddr.sin_port        = htons(UDP_PORT);

	if ( bind(sockfd, (struct sockaddr *) &nf_servaddr, sizeof(nf_servaddr)) < 0)
	   { log_error("bind UDP error"); perror("bind UDP error"); }

	SetUDPOpts(sockfd, bufsize);

	syslog(LOG_INFO, "Listening on UDP port %d\n",UDP_PORT);
	
	signal(SIGPIPE,SIG_IGN);
	signal(SIGWINCH,SIG_IGN);
	signal(SIGUSR1,&callsig);
	signal(SIGUSR2,&callsig);
	signal(SIGINT,&callsig);
	signal(SIGTERM,&callsig);
	signal(SIGQUIT,&callsig);

	if (!not_detalized) create_dump_file_tmp();

	/* OK, all the network stuff worked (I think).  Now become a daemon (unless we got -d)*/
	if (debug==0 && debug2==0)
	{
		pid_t pid;

		printf("Forking into background.  Future messages will be sent to syslog\n");
		use_syslog=1;
	
		daemon(1,0);
	}
 	
	fd_set read_fds, write_fds;

   	//Ignore all signals for pselect()
	sigset_t sigmask;
	sigfillset(&sigmask);
	
	while (1) processmessage(sockfd);

	
} // end main

void processmessage(int sockfd) {
	int c,i;
	char mesg[MAXPACKET];
	struct flow_ver5_hdr * flowhdr;
	struct flow_ver5_rec * flowrec;

	/* read header */
	c=read_n(sockfd,mesg,MAXPACKET);

	if (c == 0)
	{
		log_error("UDP port connection closed");	
		exit(1);
	}
	
	if (c < 0 ) 
	{
		log_error("Can't read data from port");
		exit(1);
	}

	if (c != MAXPACKET)
	{
		log_warning("Short netflow packet");
		return;
	}
	
	flowhdr=(struct flow_ver5_hdr *)((void *)mesg);
	/* change header from network to host byte order */
	flowhdr->version=ntohs(flowhdr->version);
	flowhdr->count=ntohs(flowhdr->count);
	flowhdr->uptime=ntohl(flowhdr->uptime);
	flowhdr->unix_secs=ntohl(flowhdr->unix_secs);
	flowhdr->unix_nsecs=ntohl(flowhdr->unix_nsecs);
	flowhdr->flow_sequence=ntohl(flowhdr->flow_sequence);

	if (debug){ printf("%d flows in packet\n",flowhdr->count); }

	if (flowhdr->version != 5) 
	{
		syslog(LOG_INFO, "Bad netflow packet.  Version is %d, should be 5.\n",flowhdr->version);
		if (debug) printf("Bad netflow packet.  Version is %d, should be 5.\n",flowhdr->version);
		return;
	}
	
	if ( (flowhdr->count <1) || (flowhdr->count>1000))
	{
		syslog(LOG_INFO, "Strange netflow packet.  Record count is %d\n",flowhdr->count);
		if (debug) printf("Strange netflow packet.  Record count is %d\n",flowhdr->count);
		return;
	}

	if (debug) printf("read %d bytes of records\n",c);

	int t = flowhdr->unix_secs;
	int f = flowhdr->flow_sequence;
	
	for (i=0; i<(flowhdr->count); i++)
	{
		if (debug) printf("flow_item: %d\n",i);

		flowrec=(struct flow_ver5_rec *)(((void *)flowhdr + sizeof(struct flow_ver5_hdr))
		    	                                          + (i*sizeof(struct flow_ver5_rec)));

		/* change record from network to host byte order */
		flowrec->input_index=ntohs(flowrec->input_index);
		flowrec->output_index=ntohs(flowrec->output_index);
		flowrec->dPkts=ntohl(flowrec->dPkts);
		flowrec->dOctets=ntohl(flowrec->dOctets);
		flowrec->First=ntohl(flowrec->First);
		flowrec->Last=ntohl(flowrec->Last);
		flowrec->srcport=htons(flowrec->srcport);
		flowrec->dstport=htons(flowrec->dstport);
		flowrec->src_as=htons(flowrec->src_as);
		flowrec->dst_as=htons(flowrec->dst_as);
				  
		struct in_addr srcip,dstip;	

		srcip.s_addr=flowrec->srcaddr;
		dstip.s_addr=flowrec->dstaddr;

		struct struct_class_data * class;
	        class = (struct struct_class_data *) search_class(htonl(flowrec->srcaddr),htonl(flowrec->dstaddr));

		unsigned int class_name = 0;
		
		if ( class != CLASS_NOT_FOUND )
		{ // Class found
			if ( class->class_name != 0 )
			{
				aggregate_data(htonl(flowrec->srcaddr),htonl(flowrec->dstaddr),class->class_id,flowrec->dOctets);
			}
			else
			{
				aggregate_data(0,0,class->class_id,flowrec->dOctets);
			}
			class_name = class->class_name;
		}
	
		if (as_aggregate_on) aggregate_as(flowrec->src_as,flowrec->dst_as,flowrec->dOctets);
		
		if (!not_detalized)	
		if (human)
		{
			char * ip_src = strdup(inet_ntoa(srcip));
			char * ip_dst = strdup(inet_ntoa(dstip));
			
			fprintf(fd,"%d\t%s\t%d\t%s\t%d\t%u\t%u\n",t,ip_src,
					flowrec->srcport,ip_dst,
					flowrec->dstport,flowrec->dOctets,class_name);
			free(ip_src);
			free(ip_dst);
		}
		else
		{
			fprintf(fd,"%d\t%u\t%d\t%u\t%d\t%u\t%u\n",t,htonl(flowrec->srcaddr),
				flowrec->srcport,htonl(flowrec->dstaddr),
				flowrec->dstport,flowrec->dOctets,class_name);
		}	


		if (debug2)
		{
			char * ip_src = strdup(inet_ntoa(srcip));
			char * ip_dst = strdup(inet_ntoa(dstip));
			
			printf("%d\t%s\t%d\t%s\t%d\t%u\t%u\n",t,ip_src,
					flowrec->srcport,ip_dst,
					flowrec->dstport,flowrec->dOctets,class_name);
			free(ip_src);
			free(ip_dst);
		}
	}
}

ssize_t read_n(int fd, void *vptr, size_t n)
{
	size_t  nleft;
	ssize_t nread;
	char    *ptr;

	ptr = vptr;
	nleft = n;
	
	while (nleft > 0) 
	{
		if ( (nread = read(fd, ptr, nleft)) < 0) 
		{
			if (errno == EINTR) nread = 0; else return(-1);
	        } else if (nread == 0)break;
		nleft -= nread;
		ptr   += nread;
	}
	return(n - nleft);
}


 /* Increase our receive buffer for sockfd: */
int SetUDPOpts(int fd, int rcvsize){
	int r,p;
	socklen_t       optlen;
  
	optlen = sizeof(p);
  
	if (debug)
	{
		r=getsockopt(fd,SOL_SOCKET,SO_RCVBUF,&p,&optlen);
		printf("Before setsockopt, SO_RCVBUF is %d\n",p);
	}

	if (setsockopt(fd,SOL_SOCKET,SO_RCVBUF,&rcvsize,sizeof(rcvsize)) < 0) {
		perror("setsockopt SO_RCVBUF:");
	}

	r=getsockopt(fd,SOL_SOCKET,SO_RCVBUF,&p,&optlen);
	syslog(LOG_INFO,"Setsockopt, SO_RCVBUF is %d\n",p);
	
	if (debug) printf("After setsockopt, SO_RCVBUF is %d\n",p);
	
	return 0;
}

void log_warning(const char *warndescr)
{
	if (use_syslog) syslog(LOG_WARNING, "%s: %m", warndescr); 
		else fprintf(stderr, "%s: %s\n", warndescr, strerror(errno));
}
        
void log_error(const char * errdescr)
{
	if (use_syslog)	
	{
		syslog(LOG_ERR,"%s: %m", errdescr); 
		syslog(LOG_INFO, "Shutting down utm-netflow");
	}
	else
	{
		perror(errdescr);
	}
	exit(1);
}


