// Written by DaNuk (DNk) 02-03-2006
// mail@danuk.ru

#include <string.h>
#include <stddef.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#include "config.h"
#include "utm-classes.h"
#include "utm-data.h"

#define CONFIG_LINE_SIZE 256
#define MAX_TOKENS 100

extern char * BIND_ADDRESS;
extern int UDP_PORT;

extern int nets_aggregate[];
extern int nets_aggregate_index;

extern int as_aggregate_on;

const char delimiters[] = " 	/";

void init_config(char * config_file)
{
	int fd = open(config_file,O_RDONLY);
	
	if (fd<1)
	{
		fprintf(stderr,"Can't open config file: <%s>\n",config_file); 
		exit(1);
	}

	char * config_data = (char*) malloc(sizeof(char)*CONFIG_LINE_SIZE+1);
	config_data[CONFIG_LINE_SIZE]=0;

	int n_line=0;
	
	while (read_line(fd, config_data, CONFIG_LINE_SIZE))
	{
		n_line++;

		char *token;
		// Skip if not token
		if ((token = strtok(config_data, delimiters))==NULL) continue;

		if (strcasecmp(token,"LISTEN")==0)
		{
			int tokens[2];
			int count = get_tokens(tokens,2);			

			if (strcasecmp((char*)tokens[0],"ANY")==0)
				BIND_ADDRESS = 0;
			else	BIND_ADDRESS = strdup((char*)tokens[0]);
			
			UDP_PORT = atoi((char*)tokens[1]);
		}
       		else if (strcasecmp(token,"CLASS")==0)
		{
			int tokens[5];
			int count = get_tokens(tokens,5);
			int direction = 0;
			
			if (count < 3)
			{
				fprintf(stderr,"Incorrect format directive CLASS:\nCLASS <ID> <SRC_NET> <DST_NET>\n");
				exit(1);
			}

			int class = atoi((char*)tokens[0]);	

			unsigned int src_ip,src_mask,dst_ip,dst_mask;
			
			src_ip = get_ip_int(tokens[1]);	
			
			if (strlen((char*)tokens[2])>2)	src_mask = get_ip_int(tokens[2]);
			else				src_mask = get_mask_from_prefix(atoi((char*)tokens[2]));

			dst_ip = get_ip_int(tokens[3]);	

			if (strlen((char*)tokens[4])>2)	dst_mask = get_ip_int(tokens[4]);
			else				dst_mask = get_mask_from_prefix(atoi((char*)tokens[4]));


			if (src_ip & src_mask != src_ip)
			{
				fprintf(stderr,"Error: incorrect network: %s\\%s\n",tokens[1],tokens[2]);
				exit(1);
			}
		
			if (dst_ip & dst_mask != dst_ip)
			{
				fprintf(stderr,"Error: incorrect network: %s\\%s\n",tokens[3],tokens[4]);
				exit(1);
			}
	
			add_class(	(unsigned int*) src_ip,(unsigned int*) src_mask,
					(unsigned int*) dst_ip,(unsigned int*) dst_mask,
					(unsigned int*) class );
		}
       		else if (strcasecmp(token,"AGGREGATE")==0)
		{
			int tokens[3];
			int count = get_tokens(tokens,3);

			if (strcasecmp((char*)tokens[0],"NET")==0)
			{
				unsigned int ip;
				unsigned int mask;
			
				ip =  get_ip_int(tokens[1]);
			
				if (strlen((char*)tokens[2])>2)	mask = get_ip_int(tokens[2]);
				else				mask = get_mask_from_prefix(atoi((char*)tokens[2]));
	
				if (ip & mask !=  ip)
				{
					fprintf(stderr,"Error: incorrect network: %s\\%s\n",tokens[1],tokens[2]);
					exit(1);
				}
			
				add_aggregate((unsigned int*)ip, (unsigned int*)mask);
			}
			else if (strcasecmp((char*)tokens[0],"AS")==0)
			{
				as_aggregate_on = 1;
			}
			else
			{
				fprintf(stderr,"Error: incorrect AGGREGATE syntax\n");
				exit(1);
			}
		}
		else
		{
			fprintf(stderr,"Error config file syntax in line: %d\n<%s>\n",n_line,config_data);
			exit(1);
		}
	}
	fd = close(fd);
}

int get_tokens(int * tokens, int cnt)
{
	char * token;
	char * str;
	int n_token=0;
	
	while ( (token = strtok(NULL, delimiters))!=NULL )
	{
		if (n_token == cnt)
		{
			fprintf(stderr,"Error!\n");
			exit(1);
		}
		
		tokens[n_token] = (int) token;
		n_token++;
	}
	return n_token;
}

int read_line(int log_fd, char * log_buf, int log_line_size)
{
	char buf[1];
	int read_bytes=0;
	int length=0;
	int comment = 0;
	
	log_buf[0]=0;
	while ( strncmp(buf,"\n",1)!=0 )
	{
		if (!(read_bytes = read(log_fd, buf, 1))) break;
		if (strncmp(buf,"\n",1) == 0) break;      // Skip empty string
		if (strncmp(buf,"#",1) == 0) comment = 1; // Set comment flag

		if (!comment)
		{
			strncat(log_buf, buf, read_bytes);
			length+=read_bytes;
		}
	
		if (length == log_line_size) break;	
	}
	log_buf[length]=0;
	return read_bytes;
}

int get_mask_from_prefix(int prefix)
{
	int mask = power(2,prefix) - 1 << 32 - prefix;
	return mask;
}

int power(int base, int exp)
{
	int answer;
	for (answer = 1; exp > 0; exp--) answer *= base;
	return answer;
}

