// utm-netflow 2.0.0 Writen by DaNuk (DNk) 12-03-2008
// mail@danuk.ru

#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdlib.h>

#include "config.h"
#include "utm-classes.h"
#include "utm-data.h"

extern int human;
extern int debug;
FILE * FILE_DUMP_TMP_FD = NULL;

extern int count_classes;
extern unsigned int c_name[];

void log_error(const char *);
void log_warning(const char *);

int nets_aggregate[MAX_AGGREGATE_NETS];
    
int nets_aggregate_index = 0;

void open_file_dump_tmp();

void add_aggregate(unsigned int * ip, unsigned int * mask)
{
	if (MAX_AGGREGATE_NETS == nets_aggregate_index)
	{
		fprintf(stderr,"Error: MAX_AGGREGATE_NETS reached\n");
		exit(1);
	}

	struct struct_nets_aggregate * aggregate;

	aggregate = malloc(sizeof(struct struct_nets_aggregate));

	aggregate->ip = (unsigned int) ip;
	aggregate->mask = (unsigned int) mask;
	
	nets_aggregate[nets_aggregate_index] = (int) aggregate;
	nets_aggregate_index++;
	nets_aggregate[nets_aggregate_index]=0;	
}

void init_data()
{
	struct struct_nets_aggregate * aggregate;	

	int i=0;
	
	while (nets_aggregate[i]!=0)
	{
		aggregate = (struct struct_nets_aggregate *) nets_aggregate[i];	
	
		unsigned int net_size = (aggregate->ip |~ aggregate->mask) - aggregate->ip + 1;

		unsigned int size_bytes = net_size * count_classes * sizeof(unsigned long long);

		fprintf(stderr, "Allocated memory size for 'NET' cache: %u bytes\n",size_bytes);
		
		aggregate->ptr = calloc(net_size * count_classes,sizeof(unsigned long long));

		if (aggregate->ptr == NULL)
		{	
			fprintf(stderr,"Error: Can't allocated memory size: %u bytes.\n",size_bytes);
			exit(1);
		}
		
		i++;
	}
}

void aggregate_data(unsigned int * srcip, unsigned int * dstip, unsigned int * class, unsigned int * bytes)
{	
	struct struct_nets_aggregate * aggregate;	

	unsigned int offset;
	
	int i=0;
	
	while (nets_aggregate[i]!=0)
	{
		aggregate = (struct struct_nets_aggregate *) nets_aggregate[i];	

			
		if (((int)srcip & aggregate->mask) == aggregate->ip) 
		{
			offset = (unsigned int) srcip - (unsigned int)aggregate->ip;
			store_data(aggregate->ptr, offset, class, bytes);
		
		}
		
		if (((int)dstip & aggregate->mask) == aggregate->ip)
		{	
			offset = (unsigned int) dstip - (unsigned int)aggregate->ip;
			store_data(aggregate->ptr, offset, class, bytes);
		}

		i++;
	}
}

int store_data(unsigned long long * ptr, unsigned int * ip_offset, unsigned int * class_offset, unsigned int * bytes)
{
	unsigned int offset = (unsigned int)ip_offset * count_classes + (unsigned int)class_offset;

	ptr[offset] += (unsigned int) bytes;
}

void write_data_tmp()
{
	if (debug) printf("Write data to file...");

	open_file_dump_tmp();
	
	struct struct_nets_aggregate * aggregate;	

	int i=0;

	unsigned long long * ptr;
	unsigned int net_size;
	
	unsigned int ip_offset=0;
	unsigned int class_offset=0;
	
	while (nets_aggregate[i]!=0)
	{
		aggregate = (struct struct_nets_aggregate *) nets_aggregate[i];	
		
		net_size = (aggregate->ip |~ aggregate->mask) - aggregate->ip + 1;

		for (ip_offset=0; ip_offset<net_size; ip_offset++)
		for (class_offset=0; class_offset<count_classes; class_offset++)
		{		
			ptr = (unsigned long long*) aggregate->ptr + ip_offset * count_classes + class_offset;

			if (*ptr>0)
			{
				write_element(	(unsigned int)aggregate->ip+ip_offset, 
						(unsigned int *)class_offset,
						ptr	);
				*ptr = 0;
			}
		}		
		i++;
	}	
	
	fflush(FILE_DUMP_TMP_FD);
	
	int ret = fclose(FILE_DUMP_TMP_FD);
	if (ret != 0) log_error("Close file");

	FILE_DUMP_TMP_FD = NULL;
	
	if (debug) printf("Done\n");
}

int write_element(unsigned int * ipaddr, unsigned int * class, unsigned long long * bytes)
{
	open_file_dump_tmp();
	
	if (human)
	{
		struct in_addr ip;
		ip.s_addr = (unsigned int) htonl((unsigned int)ipaddr);
		fprintf(FILE_DUMP_TMP_FD,"%s\t%u\t%llu\n",inet_ntoa(ip),c_name[(unsigned int)class],*bytes);
	}
	else
	{
		fprintf(FILE_DUMP_TMP_FD,"%u\t%u\t%llu\n",ipaddr,c_name[(unsigned int)class],*bytes);
	}
}

void rename_data_tmp()
{
	if (FILE_DUMP_TMP_FD == NULL)
	{
		int ret = rename(FILE_DUMP_TMP,FILE_DUMP);
		if (ret != 0) log_error("Rename file");
	}
	else
	{
		log_error("Can't rename dump file\n");
	}
}

void open_file_dump_tmp()
{
	if (FILE_DUMP_TMP_FD == NULL)
	{
		FILE_DUMP_TMP_FD = fopen(FILE_DUMP_TMP, "a");
	        if (FILE_DUMP_TMP_FD == NULL) log_error("Create data file");
	}
}


